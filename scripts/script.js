// IMAGE SLIDER HOME PAGE
$(document).ready(function ($) {

    $('#checkbox').change(function () {
        setInterval(function () {
            moveRight();
        }, 3000);
    });

    var slideCount = $('#slider ul li').length;
    var slideWidth = $('#slider ul li').width();
    var slideHeight = $('#slider ul li').height();
    var sliderUlWidth = slideCount * slideWidth;

    $('#slider').css({
        width: slideWidth,
        height: slideHeight
    });

    $('#slider ul').css({
        width: sliderUlWidth,
        marginLeft: -slideWidth
    });

    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: +slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: -slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

});

// DOCUMENT UPLOAD
$(document).ready(function () {


    $('.upload-1 input[type="file"]').on('change', function () {
        $('.upload-path-1').val(this.value.replace('C:\\fakepath\\', ''));
    });
    $('.upload-2 input[type="file"]').on('change', function () {
        $('.upload-path-2').val(this.value.replace('C:\\fakepath\\', ''));
    });
    $('.upload-3 input[type="file"]').on('change', function () {
        $('.upload-path-3').val(this.value.replace('C:\\fakepath\\', ''));
    });
    $('.upload-4 input[type="file"]').on('change', function () {
        $('.upload-path-4').val(this.value.replace('C:\\fakepath\\', ''));
    });

})




//PAGINATION Activity Log History

$(document).ready(function () {
    $('#table-log-table-rows').after(
        '<div id="table-log-table-rows-nav" style="margin-left: 45%;margin-top:1%;"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#table-log-table-rows tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#table-log-table-rows-nav').append(
            '<a style="font-size:150%;padding-top:20px;order-style: dotted;"  rel="' + i + '">' +
            pageNum + ' </a>');
    }
    $('#table-log-table-rows tbody tr').hide();
    $('#table-log-table-rows tbody tr').slice(0, rowsShown).show();
    $('#table-log-table-rows a:first').addClass('active');
    $('#table-log-table-rows-nav a').bind('click', function () {

        $('#table-log-table-rows-nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#table-log-table-rows tbody tr').css('opacity', '0.0').hide().slice(startItem,
            endItem).
        css('display', 'table-row').animate({
            opacity: 1
        }, 300);
    });
});



// ROTATE ARROW MODAL
$(document).ready(function () {
    $('a.test').click(function () {
        document.getElementById("rotateArrow").classList.toggle("rotated-image");

    });
});

// Tabs in Card
// $(document).ready(function ($) {
//     $('.tab_content').hide();
//     $('.tab`_`content:first').show();
//     $('.tabs li:first').addClass('active');
//     $('.tabs li').click(function (event) {
//         $('.tabs li').removeClass('active');
//         $(this).addClass('active');
//         $('.tab_content').hide();

//         var selectTab = $(this).find('a').attr("href");

//         $(selectTab).fadeIn();
//     });
// });


// TABS IN DEPOSIT PAGE
$(document).ready(function () {

    $('ul.deposit-tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.deposit-tabs li').removeClass('current-deposit');
        $('.tab-content-deposit').removeClass('current-deposit');

        $(this).addClass('current-deposit');
        $("#" + tab_id).addClass('current-deposit');
    })

})



//PAGINATION Deposit History
$(document).ready(function () {
    $('#deposit-history-table-rows').after(
        '<div id="deposit-history-table-rows-nav" style="margin-left: 45%;margin-top:1%;"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#deposit-history-table-rows tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#deposit-history-table-rows-nav').append(
            '<a style="font-size:150%;padding-top:20px;order-style: dotted;"  rel="' + i + '">' +
            pageNum + ' </a>');
    }
    $('#deposit-history-table-rows tbody tr').hide();
    $('#deposit-history-table-rows tbody tr').slice(0, rowsShown).show();
    $('#deposit-history-table-rows a:first').addClass('active');
    $('#deposit-history-table-rows-nav a').bind('click', function () {

        $('#deposit-history-table-rows-nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#deposit-history-table-rows tbody tr').css('opacity', '0.0').hide().slice(startItem,
            endItem).
        css('display', 'table-row').animate({
            opacity: 1
        }, 300);
    });
});

//PAGINATION Withdraw History   
$(document).ready(function () {
    $('#withdraw-history-table-rows').after(
        '<div id="withdraw-history-table-rows-nav" style="margin-left: 45%;margin-top:1%;"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#deposit-history-table-rows tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#withdraw-history-table-rows-nav').append(
            '<a style="font-size:150%;padding-top:20px;order-style: dotted;"  rel="' + i + '">' +
            pageNum + ' </a>');
    }
    $('#withdraw-history-table-rows tbody tr').hide();
    $('#withdraw-history-table-rows tbody tr').slice(0, rowsShown).show();
    $('#withdraw-history-table-rows a:first').addClass('active');
    $('#withdraw-history-table-rows-nav a').bind('click', function () {

        $('#withdraw-history-table-rows-nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#withdraw-history-table-rows tbody tr').css('opacity', '0.0').hide().slice(startItem,
            endItem).
        css('display', 'table-row').animate({
            opacity: 1
        }, 300);
    });
});

// Tabs For Trading page
$(document).ready(function () {

    $('ul.tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    })


    // tabs For Deals
    $('ul.deals-tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.deals-tabs li').removeClass('current-deals');
        $('.tab-content-deals').removeClass('current-deals');

        $(this).addClass('current-deals');
        $("#" + tab_id).addClass('current-deals');
    })




})

// Accordion For Modal
$(document).ready(function () {
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener(
            "click",
            function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
    }



})


//   Customer index.html
// CURRENCIES TABLE FIXED TABLE HEADER
$(document).ready(function () {
    $(".main-table").clone(true).appendTo('#table-scroll .faux-table').addClass('clone');
    $(".main-table.clone").clone(true).appendTo('#table-scroll .faux-table').addClass('clone2');
});

// Open-deals TABLE FIXED TABLE HEADER

$(document).ready(function () {
    $(".open-deals-main-table").clone(true).appendTo('#open-deals-table-scroll .open-deals-faux-table')
        .addClass('clone');
    $(".open-deals-main-table.clone").clone(true).appendTo(
        '#open-deals-table-scroll .open-deals-faux-table').addClass('clone2');
});


$(document).ready(function () {
    $('#open-deals-table-rows').after(
        '<div id="open-deals-table-rows-nav" style="margin-left: 45%;margin-top:1%;"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#open-deals-table-rows tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#open-deals-table-rows-nav').append(
            '<a id="page-no-pagination" style="font-size:150%;padding-top:20px;"  rel="' + i + '">' +
            pageNum + ' </a>');
    }
    $('#open-deals-table-rows tbody tr').hide();
    $('#open-deals-table-rows tbody tr').slice(0, rowsShown).show();
    $('#open-deals-table-rows-nav a:first').addClass('active');
    $('#open-deals-table-rows-nav a').bind('click', function () {

        $('#open-deals-table-rows-nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#open-deals-table-rows tbody tr').css('opacity', '0.0').hide().slice(startItem,
            endItem).
        css('display', 'table-row').animate({
            opacity: 1
        }, 300);
    });
});



//Closed deals
$(document).ready(function () {
    $('#close-deals-table-rows').after(
        '<div id="close-deals-table-rows-nav" style="margin-left: 45%;margin-top:1%;"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#close-deals-table-rows tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#close-deals-table-rows-nav').append(
            '<a style="font-size:150%;padding-top:20px;order-style: dotted;"  rel="' + i + '">' +
            pageNum + ' </a>');
    }
    $('#close-deals-table-rows tbody tr').hide();
    $('#close-deals-table-rows tbody tr').slice(0, rowsShown).show();
    $('#close-deals-table-rows-nav a:first').addClass('active');
    $('#close-deals-table-rows-nav a').bind('click', function () {

        $('#close-deals-table-rows-nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#close-deals-table-rows tbody tr').css('opacity', '0.0').hide().slice(startItem,
            endItem).
        css('display', 'table-row').animate({
            opacity: 1
        }, 300);
    });
});

// ----------------MODAL ROW SELECTION-----------------------------------------------

$(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.location = "#modal-one";
    });
});

// ------------------------------------------------------------------------

// Date Picker in profile info
$(document).ready(function () {
    $('#date-of-birth').datepicker({});
});
$(document).ready(function () {
    $('.from').datepicker({});
});
$(document).ready(function () {
    $('.to').datepicker({});
});


// Active BUY SELL Modal
document.getElementById("ordered").style.opacity = "0.2";

function sellActive() {
    document.getElementById("ordered").disabled = false;
    document.getElementById("sell-card").style.border = "1px solid black";
    document.getElementById("buy-card").style.border = "0px none black";
    document.getElementById("ordered").style.opacity = "10";
}
function buyActive() {
    document.getElementById("ordered").disabled = false;
    document.getElementById("buy-card").style.border = "1px solid black";
    document.getElementById("sell-card").style.border = "0px none black";
    document.getElementById("ordered").style.opacity = "10";
}

function modalReset() {
    document.getElementById("ordered").style.opacity = "0.2";

    document.getElementById("ordered").disabled = true;
    document.getElementById("buy-card").style.border = "0px none black";
    document.getElementById("sell-card").style.border = "0px none black";


}
// ON ESCAPE KEY MODAL CLOSE

